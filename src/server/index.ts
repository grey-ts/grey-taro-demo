
import Taro from "@tarojs/taro";

const DOMAIN = 'http://xxxx.xxx.xxxx';
const CONTEXT = '/aaa/vvv'

export interface IParameter {
}

export async function request(option: Taro.request.Option, parameter?: IParameter) {


    for (let i = 0; i < preProcessing.length; i++) {
        preProcessing[i](option, parameter)
    }

    const task = await Taro.request({
        ...option,
        url: `${DOMAIN}${CONTEXT}${option.url}`
    });

    postProcessing.forEach(fn => { fn(task, option, parameter) });

    return task;
}

type IPreProcessing = (option: Taro.request.Option, parameter?: IParameter) => void;

type IPostProcessing = (task: Taro.request.SuccessCallbackResult, option: Taro.request.Option, parameter?: IParameter) => void;


/** 前期处理 */
const preProcessing: IPreProcessing[] = [

]


/** 后期处理 */
const postProcessing: IPostProcessing[] = [
]