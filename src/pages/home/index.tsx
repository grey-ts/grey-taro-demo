import { request } from '@/server'
import { Button, View } from '@tarojs/components'
import React from 'react'


export default function () {
  const onClick = React.useCallback(() => {
    request({ url: '/cccc', method: 'GET', data: { d: 1 } })
  }, [])

  return (
    <View className='index'>
      首页
      <Button onClick={onClick}>发送</Button>
    </View>
  )
}
