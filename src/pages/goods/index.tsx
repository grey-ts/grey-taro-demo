import { Form, Switch, View } from '@tarojs/components'


export default function () {

  const formSubmit = e => {
    console.log(e)
  }

  const formReset = e => {
    console.log(e)
  }

  return (
    <View className='index'>
      <View></View>
      <Form onSubmit={formSubmit} onReset={formReset} >
        <View className='example-body'>
          <Switch name='switch' className='form-switch'></Switch>
        </View>
      </Form>
    </View>
  )
}
