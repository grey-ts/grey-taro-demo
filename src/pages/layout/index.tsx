
import React from 'react';
import { View } from '@tarojs/components'
import Home from '@/pages/home'
import Goods from '@/pages/goods'
import Share from '@/pages/share'
import My from '@/pages/my'
import * as UI from './ui'
import I001 from './imgs/I001.svg';
import I002 from './imgs/I002.svg';
import I003 from './imgs/I003.svg';
import I004 from './imgs/I004.svg';
import './index.less'

export default function Index() {

  const [selectedKey, setSelectedKey] = React.useState<string | number>(1);

  const columns: (UI.ITabbarColumns & { element: JSX.Element })[] = React.useMemo(() => {
    return [
      {
        label: '首页',
        icon: I001,
        key: 1,
        element: <Home />
      }, {
        label: '产品',
        icon: I002,
        key: 2,
        element: <Goods />
      }, {
        label: '分享',
        icon: I003,
        key: 3,
        element: <Share />
      }, {
        label: '我的',
        icon: I004,
        key: 4,
        element: <My />
      },
    ]
  }, [])

  return (
    <View className='index'>
      <View className='body'>
        {columns.filter(e => e.key === selectedKey)[0]?.element}
      </View>
      <UI.Tabbar columns={columns} value={selectedKey} onChange={setSelectedKey} />
    </View>
  )
}
