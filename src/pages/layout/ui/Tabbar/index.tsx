
import { Image, View } from "@tarojs/components";
import classnames from "classnames";
import './index.less'

export interface ITabbarColumns {
    /** 图标 */
    icon: string;

    /** 名称 */
    label: string;

    /** 标记 */
    key: string | number;
}

export interface ITabbarProps {
    /** 配置 */
    columns: ITabbarColumns[];

    /** 当前 */
    value?: string | number;

    /** 改变 */
    onChange: (e: string | number) => void
}

export function Tabbar(props: ITabbarProps) {

    const { columns, value, onChange } = props;

    return (
        <View className='tabbar' >
            {
                columns.map(e => {
                    return (
                        <View key={e.key} className={classnames('tabbar-item', { 'on': value === e.key })} onClick={() => { onChange(e.key) }}>
                            <Image src={e.icon} />
                            <View>{e.label}</View>
                        </View>
                    )
                })
            }
        </View>
    )
}